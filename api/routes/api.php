<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('login', 'AuthController@login');

Route::group(['middleware' => ['jwt.verify']], function() {

	Route::get('logout', 'AuthController@logout');
	Route::post('refresh', 'AuthController@refresh');
	Route::get('me', 'AuthController@me');

	Route::apiResource('user', 'UserController')->middleware('admin');
	Route::apiResource('client', 'ClientController');
	Route::apiResource('contact', 'ContactController');
	Route::apiResource('meeting', 'MeetingController');
	Route::apiResource('support', 'SupportController');
});