<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
    	DB::table('users')->insert([
            'name' => 'Esteban',
            'lastnames' => 'Salas Guzman',
            'username' => 'Admin',
            'password' => Hash::make('isw7112020'),
            'admin' => true,
        ]);
    }
}
