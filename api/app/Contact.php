<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	protected $fillable = [
		'client_id', 'name', 'lastnames', 'email', 'phone_number', 'job',
	];

	public function client()
	{
		return $this->belongsTo('App\Client');
	}
}
