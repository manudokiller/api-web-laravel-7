<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;

class ClientController extends Controller
{
    //get an client or fail
    private function getClientOrFail($id)
    {
        try {
            $client = Client::find($id);
        } catch (QueryException $e) {
            return ['errors' => ['Something went wrong getting client ' . $id]];
        }
        if (!$client) {
            return ['errors' => ['Client not found']];
        }
        return $client;
    }

    //send all clients
    public function index()
    {
        try {
            $clients = Client::all();
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong getting all clients ', 
            ]]);
        }
        return response()->json([
            'clients' => $clients
        ], 200);
    }

    //create a new client
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:clients,name',
            'legal_certificate' => 'required|numeric|digits:10|unique:clients,legal_certificate',
            'website' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => 'required|numeric|digits:8',
            'sector' => 'required|in:educacion,industria,agricultura,manufactura,servicios,otros'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        try {
            $client = Client::firstOrCreate([
                'name' => $request->get('name'),
                'legal_certificate' => $request->get('legal_certificate'),
                'website' => $request->get('website'),
                'address' => $request->get('address'),
                'phone_number' => $request->get('phone_number'),
                'sector' => $request->get('sector')
            ]);
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong creating the client ', 
            ]]);
        }
        return response()->json(['client' => $client], 201);
    }

    //send a specific client
    public function show($id)
    {
        $client = $this->getClientOrFail($id);
        if(is_array($client)){
          return response()->json($client);//send errors
        }
        return response()->json(['client' => $client], 200);
    }

    //update a specific client
    public function update(Request $request, $id)
    {
        $client = $this->getClientOrFail($id);
        if(is_array($client)){
          return response()->json($client);//send errors
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:clients,name,' . $id . ',id',
            'legal_certificate' => 'required|digits:10|unique:clients,legal_certificate,' . $id . ',id',
            'website' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => 'required|digits:8',
            'sector' => 'required|in:educacion,industria,agricultura,manufactura,servicios,otros'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        try {
            $client->update($request->all());
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong getting client ' . $id, 
            ]]);
        }
        return response()->json(['client' => $client], 200);
    }

    //delete a specific client
    public function destroy($id)
    {
        $client = $this->getClientOrFail($id);
        if(is_array($client)){
          return response()->json($client);//send errors
        }
        try {
            Client::destroy($id);
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong deleting client ' . $id, 
            ]]);
        }
        return response()->json(['message' => 'client deleted correctly', 'client' => $client], 200);
    }
}
