<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;

class ContactController extends Controller
{
    //get an contact or fail
    private function getContactOrFail($id)
    {
        try {
            $contact = Contact::find($id);
        } catch (QueryException $e) {
            return ['errors' => ['Something went wrong getting contact ' . $id]];
        }
        if (!$contact) {
            return ['errors' => ['contact not found']];
        }
        return $contact;
    }

    //send all contacts
    public function index()
    {
        try {
            $contacts = Contact::all();
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong getting all contacts ', 
            ]]);
        }
        $clients = array();
        if (!empty($contacts)) {
            foreach ($contacts as $contact) {
                if ($contact->client()->exists()) {
                    $clients = array_merge([$contact->client->id . ' ' => $contact->client->name], $clients);
                }
            }
        }
        return response()->json([
            'contacts' => $contacts,
            'clients' => $clients
        ], 200);
    }

    //create a new contact
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|exists:clients,id',
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'email' => 'required|max:255|email|unique:contacts,email',
            'phone_number' => 'required|digits:8',
            'job' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        try {
            $contact = Contact::firstOrCreate([
                'client_id' => $request->get('client_id'),
                'name' => $request->get('name'),
                'lastnames' => $request->get('lastnames'),
                'email' => $request->get('email'),
                'phone_number' => $request->get('phone_number'),
                'job' => $request->get('job')
            ]);
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong creating the contact ', 
            ]]);
        }
        return response()->json(['contact' => $contact], 201);
    }

    //send a specific contact
    public function show($id)
    {
        $contact = $this->getContactOrFail($id);
        if(is_array($contact)){
          return response()->json($contact);//send errors
        }
        return response()->json(['contact' => $contact], 200);
    }

    //update a specific contact
    public function update(Request $request, $id)
    {
        $contact = $this->getContactOrFail($id);
        if(is_array($contact)){
          return response()->json($contact);//send errors
        }
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|exists:clients,id',
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'email' => 'required|max:255|email|unique:contacts,email,' . $id . ',id',
            'phone_number' => 'required|digits:8',
            'job' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        try {
            $contact->update($request->all());
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong getting contact ' . $id, 
            ]]);
        }
        return response()->json(['contact' => $contact], 200);
    }

    //delete a specific contact
    public function destroy($id)
    {
        $contact = $this->getContactOrFail($id);
        if(is_array($contact)){
          return response()->json($contact);//send errors
        }
        try {
            Contact::destroy($id);
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong deleting contact ' . $id, 
            ]]);
        }
        return response()->json(['message' => 'Contact deleted correctly', 'contact' => $contact], 200);
    }
}
