<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    //return an array of users meetings (title each)
    private function meetings_users($users)
    {
        $meetings = array();
        foreach ($users as $user) {
            if ($user->meetings()->exists()) {
                $user_meetings = '';
                foreach ($user->meetings as $meeting) {
                    $user_meetings .= $meeting['title'] . ' | ';
                }
                if ($user_meetings) {
                    $meetings[$user['id']] = $user_meetings;
                }
            }
        }
        return $meetings;
    }

    //get an user or fail
    private function getUserOrFail($id)
    {
        try {
            $user = User::find($id);
        } catch (QueryException $e) {
            return ['errors' => ['Something went wrong getting user ' . $id]];
        }
        if (!$user) {
            return ['errors' => ['User not found']];
        }
        return $user;
    }

    //send all users
    public function index()
    {
        try {
            $users = User::all();
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong getting all Users ', 
            ]]);
        }
        return response()->json([
            'users' => $users,
            'meetings' => $this->meetings_users($users)
        ], 200);
    }

    //create a new user
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'username' => 'required|max:255|unique:users,username',
            'password' => 'required|max:255|min:8',
            'admin' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        try {
            $user = User::firstOrCreate([
                'name' => $request->get('name'),
                'lastnames' => $request->get('lastnames'),
                'username' => $request->get('username'),
                'password' => Hash::make($request->get('password')),
                'admin' => $request->get('admin'),
            ]);
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong creating the user ', 
            ]]);
        }
        return response()->json(['user' => $user], 201);
    }

    //send a specific user
    public function show($id)
    {
        $user = $this->getUserOrFail($id);
        if(is_array($user)){
          return response()->json($user);//send errors
        }
        return response()->json(['user' => $user], 200);
    }

    //update a specific user
    public function update(Request $request, $id)
    {
        $user = $this->getUserOrFail($id);
        if(is_array($user)){
          return response()->json($user);//send errors
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'username' => 'required|max:255|unique:users,username,' . $id . ',id',
            'password' => 'required|max:255|min:8',
            'admin' => 'required|boolean',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        if ($user->username == 'Admin') {
            if ($user->username != $request->get('username')) {
                return response()->json(['errors' => 'You can not modify Admin username']);
            }
            if (!$request->get('admin')) {
                return response()->json(['errors' => 'You can not modify Admin role']);
            }
        }
        try {
            $user->update([
                'name' => $request->get('name'),
                'lastnames' => $request->get('lastnames'),
                'username' => $request->get('username'),
                'password' => Hash::make($request->get('password')),
                'admin' => $request->get('admin'),
            ]);
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong getting user ' . $id, 
            ]]);
        }
        return response()->json(['user' => $user], 200);
    }

    //delete a specific user
    public function destroy($id)
    {
        $user = $this->getUserOrFail($id);
        if(is_array($user)){
          return response()->json($user);//send errors
        }
        if ($user->username == 'Admin') {
            return response()->json(['errors' => ['You can not delete Admin user']]);
        }
        try {
            if ($user->meetings()->exists()) {
                $user->meetings()->detach();
            }
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong detaching meetings from user ' . $id, 
            ]]);
        }
        try {
            User::destroy($id);
        } catch (QueryException $e) {
            return response()->json(['errors' => [
                'Something went wrong deleting user ' . $id, 
            ]]);
        }
        return response()->json([
            'message' => 'User deleted correctly', 
            'user' => $user
        ], 200);
    }
}
