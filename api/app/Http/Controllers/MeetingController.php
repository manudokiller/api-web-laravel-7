<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meeting;
use App\User;

class MeetingController extends Controller
{
    private function meeting_users($meeting)
    {
        $users = '';
        foreach ($meeting->users as $user) {
            $users .= $user->username . ' - ';
        }
        return substr($users, 0, -3);
    }

    private function meetings_users($meetings)
    {
        $meetings_users = array();
        foreach ($meetings as $meeting) {
            if ($meeting->users()->exists()) {
                $meetings_users = array_merge($meetings_users, [$meeting->id . ' ' => $this->meeting_users($meeting)]);
            }
        }
        return $meetings_users;
    }

    public function index()
    {
        try {
            $meetings = Meeting::all();
        } catch (QueryException $e) {
            return response()->json(['errors' => ['Something went wrong getting all Meetings']]);
        }
        $clients = array();
        if (!empty($meetings)) {
            foreach ($meetings as $meeting) {
                if ($meeting->client()->exists()) {
                    $clients = array_merge([$meeting->client->id . ' ' => $meeting->client->name], $clients);
                }
            }
        }
        return response()->json([
            'meetings' => $meetings,
            'meetings_users' =>  $this->meetings_users($meetings),
            'clients' => $clients
        ], 200);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'date' => 'required|date|after:today',
            'time' => 'required|date_format:H:i',
            'virtual' => 'in:on,null',
            'user_ids' => 'required|array'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        if ($request['client_id'] != 'null') {
            $validator = Validator::make($request->all(), [
                'client_id' => 'exists:clients,id',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->messages()]);
            }
        } else {
            $request['client_id'] = null;
        }
        $date_time = date('Y-m-d H:i:s', strtotime($request->get('date') . ' ' . $request->get('time')));
        if ($request->get('virtual')) {
            $request['virtual'] = '1';
        }
        try {
            $meeting = Meeting::firstOrCreate([
                'client_id' => $request->get('client_id'),
                'title' => $request->get('title'),
                'date_time' => $date_time,
                'virtual' => $request->get('virtual')
            ]);
        } catch (QueryException $e) {
            return response()->json(['errors' => ['Something went wrong creating the Meeting']]);
        }
        foreach ($request->get('user_ids') as $user_id) {
            try {
                $user = User::find($user_id);
            } catch (QueryException $e) {
                return response()->json(['errors' => ['Something went wrong getting user ' . $user_id]]);
            }
            if (!$user) {
                return response()->json(['errors' => ['User ' . $user_id . ' selected not found']]);
            }
            try {
                $meeting->users()->attach($user);
            } catch (QueryException $e) {
                return response()->json(['errors' => ['Something went wrong attaching user ' . $user_id . ' to meeting ' . $meeting->id]]);
            }
        }
        return response()->json([
            'meeting' => $meeting
        ], 201);
    }

    public function show($id)
    {
        try {
            $meeting = Meeting::find($id);
        } catch (QueryException $e) {
            return response()->json(['errors' => ['Something went wrong getting meeting ' . $id]]);
        }
        if (!Meeting::find($id)) {
            return response()->json(['errors' => ['Meeting not found']]);
        }
        $meeting_users = null;
        if ($meeting->users()->exists()) {
            $meeting_users = $this->meeting_users($meeting);
        }
        return response()->json([
            'meeting' => $meeting,
            'meeting_users' => $meeting_users
        ], 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $meeting = Meeting::find($id);
        } catch (QueryException $e) {
            return response()->json(['errors' => ['Something went wrong getting meeting ' . $id]]);
        }
        if (!$meeting) {
            return response()->json(['errors' => ['Meeting not found']]);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'date' => 'required|date|after:today',
            'time' => 'required|date_format:H:i',
            'virtual' => 'in:on,null',
            'user_ids' => 'required|array'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        if ($request['client_id'] != 'null') {
            $validator = Validator::make($request->all(), [
                'client_id' => 'exists:clients,id',
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->messages()]);
            }
        }
        $date_time = date('Y-m-d H:i:s', strtotime($request->get('date') . ' ' . $request->get('time')));
        $virtual = '0';
        if ($request->get('virtual')) {
            $virtual = '1';
        }
        $client_id = null;
        if ($request->get('client_id') != 'null') {
            $client_id = $request['client_id'];
        }
        try {
            $meeting->fill([
                'client_id' => $client_id,
                'title' => $request->get('title'),
                'date_time' => $date_time,
                'virtual' => $virtual
            ]);
            $meeting->save();
        } catch (QueryException $e) {
            return response()->json(['errors' => ['Something went wrong updating meeting ' . $id]]);
        }
        if ($request->get('user_ids')) {
            foreach ($request->get('user_ids') as $user_id) {
                try {
                    $user = User::find($user_id);
                } catch (QueryException $e) {
                    return response()->json(['errors' => ['Something went wrong getting user ' . $user_id]]);
                }
                if (!$user) {
                    return response()->json(['errors' => ['User ' . $user_id . ' selected not found']]);
                }
                if ($meeting->users()->exists()) {
                    if (!$meeting->users->contains($user)) {
                        try {
                            $meeting->users()->attach($user);
                        } catch (QueryException $e) {
                            return response()->json(['errors' => ['Something went wrong attaching user ' . $user_id . ' to meeting ' . $meeting->id]]);
                        }
                    }
                } else {
                    try {
                        $meeting->users()->attach($user);
                    } catch (QueryException $e) {
                        return response()->json(['errors' => ['Something went wrong attaching user ' . $user_id . ' to meeting ' . $meeting->id]]);
                    }
                }
            }
            if ($meeting->users()->exists()) {
                $users_ids = array();
                foreach ($meeting->users()->select('users.id')->get()->toArray() as $user) {
                    array_push($users_ids, $user['id']);
                }
                $to_detach = array_diff($users_ids, $request->get('user_ids'));
                if ($to_detach) {
                    foreach ($to_detach as $user) {
                        try {
                            $meeting->users()->detach($user);
                        } catch (QueryException $e) {
                            return response()->json(['errors' => ['Something went wrong detaching user ' . $user_id . ' to meeting ' . $meeting->id]]);
                        }
                    }
                }
            }
        } else {
            if ($meeting->users()->exists()) {
                $meeting->users()->detach();
            }
        }
        return response()->json([
            'meeting' => $meeting
        ], 200);
    }

    public function destroy($id)
    {
        try {
            $meeting = Meeting::find($id);
        } catch (QueryException $e) {
            return response()->json(['errors' => ['Something went wrong getting meeting ' . $id]]);
        }
        if (!$meeting) {
            return response()->json(['errors' => ['Meeting not found']]);
        }
        $aux = $meeting;
        if ($meeting->users()->exists()) {
            try {
                $meeting->users()->detach();
            } catch (QueryException $e) {
                return response()->json(['errors' => ['Something went wrong detaching users from meeting ' . $meeting->id]]);
            }
        }
        try {
            Meeting::destroy($id);
        } catch (QueryException $e) {
            return response()->json(['errors' => ['Something went wrong deleting meeting ' . $aux->id]]);
        }
        return response()->json([
            'message' => 'Meeting deleted correctly',
            'meeting' => $aux, 
        ], 200);
    }
}
