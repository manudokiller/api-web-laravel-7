<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Support;

class SupportController extends Controller
{
    public function index()
    {
        try {
            $supports = Support::all();
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Something went wrong getting all Support Tickets', 
                'error' => $e->getMessage()
            ]);
        }
        $clients = array();
        if (!empty($supports)) {
            foreach ($supports as $support) {
                if ($support->client()->exists()) {
                    $clients = array_merge([$support->client->id . ' ' => $support->client->name], $clients);
                }
            }
        }
        return response()->json([
            'supports' => $supports,
            'clients' => $clients
        ], 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|exists:clients,id',
            'title' => 'required|max:255',
            'detail' => 'required|max:255',
            'reporter' => 'required|max:255',
            'status' => 'required|in:abierto,en_proceso,en_espera,finalizado'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        try {
            $support = Support::firstOrCreate([
                'client_id' => $request->get('client_id'),
                'title' => $request->get('title'),
                'detail' => $request->get('detail'),
                'reporter' => $request->get('reporter'),
                'status' => $request->get('status')
            ]);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Something went wrong creating a new Support Ticket', 
                'error' => $e->getMessage()
            ]);
        }
        return response()->json(['support' => $support], 201);
    }

    public function show($id)
    {
        try {
            $support = Support::find($id);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Something went wrong getting support ' . $id, 
                'error' => $e->getMessage()
            ]);
        }
        if (!Support::find($id)) {
            return response()->json(['error' => 'Support not found']);
        }
        return response()->json(['support' => $support], 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $support = Support::find($id);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Something went wrong getting support ' . $id, 
                'error' => $e->getMessage()
            ]);
        }
        if (!Support::find($id)) {
            return response()->json(['error' => 'Support not found']);
        }
        $validator = Validator::make($request->all(), [
            'client_id' => 'required|exists:clients,id',
            'title' => 'required|max:255',
            'detail' => 'required|max:255',
            'reporter' => 'required|max:255',
            'status' => 'required|in:abierto,en_proceso,en_espera,finalizado'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->messages()]);
        }
        try {
            $support->update($request->all());
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Something went wrong updating support ' . $id, 
                'error' => $e->getMessage()
            ]);
        }
        
        return response()->json(['support' => $support], 200);
    }

    public function destroy($id)
    {
        try {
            $support = Support::find($id);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Something went wrong getting support ' . $id, 
                'error' => $e->getMessage()
            ]);
        }
        if (!$support) {
            return response()->json(['error' => 'Support not found']);
        }
        try {
            Support::destroy($id);
        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Something went wrong deleting support ' . $id, 
                'error' => $e->getMessage()
            ]);
        }
        
        return response()->json(['message' => 'Support deleted correctly', 'support' => $support], 200);
    }
}
