<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $fillable = [
		'name', 'legal_certificate', 'website', 'address', 'phone_number', 'sector',
	];

	public function contacts()
	{
		return $this->hasMany('App\Contact');
	}

	public function supports()
	{
		return $this->hasMany('App\Support');
	}

	public function meetings() 
	{
		return $this->hasMany('App\Meeting');
	}
}
