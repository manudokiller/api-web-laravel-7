<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
	protected $fillable = [
		'client_id', 'title', 'detail', 'reporter', 'status',
	];

	public function client()
	{
		return $this->belongsTo('App\Client');
	}
}
