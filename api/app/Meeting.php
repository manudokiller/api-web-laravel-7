<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
	protected $fillable = [
		'client_id', 'title', 'date_time', 'virtual',
	];

	public function client()
	{
		return $this->belongsTo('App\Client');
	}

	public function users()
	{
		return $this->belongsToMany('App\User');
	}
}
