# README #

This README explains how you can use this project locally.

### Clone the repositorie ###

### Go to api folder ###
* Composer update
* Change your .env file and set your database
* Migrate (php artisan migrate)
* Seed for getting the Admin user (php artisan db:seed)
* Create your JWT (php artisan jwt:secret), this will change your .env file and add there your token
* Up the server on port 8080 (php artisan serve --port=8080)

### Go to web folder ###
* Composer update
* Change your .env file and add these lines (api routes):

		API_LOGIN="http://localhost:8080/api/login"
		API_ME="http://localhost:8080/api/me"
		API_LOGOUT="http://localhost:8080/api/logout"
		API_REFRESH="http://localhost:8080/api/refresh"
		API_USER="http://localhost:8080/api/user"
		API_CLIENT="http://localhost:8080/api/client"
		API_CONTACT="http://localhost:8080/api/contact"
		API_MEETING="http://localhost:8080/api/meeting"
		API_SUPPORT="http://localhost:8080/api/support"

		GRAPHQL="http://localhost:8080/graphql"

* Up the server on port 8000 (php artisan serve)