<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Http;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Http::withToken(session('jwt'))->get(env('API_ME'))->throw()->json();
        if(array_key_exists('user', $user)){
            if (!$user['user']['admin']) {
                //return redirect()->route('auth');
                return Redirect::back()->withErrors(['User ' . $user['user']['username'] . ' doesn\'t have access']);
            }
        }
        return $next($request);
    }
}
