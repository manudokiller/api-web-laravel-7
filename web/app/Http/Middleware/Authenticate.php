<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Http;
use Closure;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        $user = Http::withToken(session('jwt'))->get(env('API_ME'))->throw()->json();
        if(!array_key_exists('user', $user)){
            return redirect()->route('auth');
        }
        return $next($request);
    }
}
