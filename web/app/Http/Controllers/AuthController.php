<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Hash;
use Auth;

class AuthController extends Controller
{
	//verify if there is an user authenticated 
	public function index()
	{
		$user = $this->get(env('API_ME'));
    if(array_key_exists('user', $user)){
      return Redirect()->route('home');
    }
		return view('auth.index');
	}

	public function login(Request $request)
	{
		$request->validate([
			'username' => 'required|max:255',
			'password' => 'required|max:255'
		]);
		$credentials = [
			'username' => $request->get('username'),
			'password' => $request->get('password')
		];
		$logged_token = $this->auth_login(env('API_LOGIN'), $credentials);
		if(!array_key_exists('token', $logged_token)){
      return Redirect::back()->withErrors($logged_token['errors']);
    }
		session(['jwt' => $logged_token['token']]);
	 	return Redirect()->route('home');
	}

	public function logout()
	{
		$this->get(env('API_LOGOUT'));
		return Redirect()->route('welcome');
	}
}
