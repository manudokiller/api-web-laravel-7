<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{

	protected $clients = [
		'report_title' => 'Clients',
		'name' => 'Name',
		'legal_certificate' => 'Legal Certificate', 
		'website' => 'Website',
		'address' => 'Address',
		'phone_number' => 'Phone Number',
		'sector' => 'Sector', 
		'contacts' => 'Contacts', 
		'supports' => 'Supports',
		'meetings' => 'Meetings',
		'created_at' => 'Created',
		'updated_at' => 'Updated'
	];

	protected $contacts = [
		'report_title' => 'Contacts',
		'client' => 'Client',
		'name' => 'Name',
		'lastnames' => 'Surnames', 
		'email' => 'Email',
		'phone_number' => 'Phone Number',
		'job' => 'Job', 
		'created_at' => 'Created',
		'updated_at' => 'Updated'
	];

	protected $supports = [
		'report_title' => 'Supports',
		'client' => 'Client',
		'title' => 'Title',
		'detail' => 'Detail', 
		'reporter' => 'Reporter',
		'status' => 'Status',
		'created_at' => 'Created',
		'updated_at' => 'Updated'
	];

	protected $meetings = [
		'report_title' => 'Meetings',
		'client' => 'Client',
		'title' => 'Title',
		'date_time' => 'Date Time', 
		'virtual' => 'Virtual',
		'users' => 'Users',
		'created_at' => 'Created',
		'updated_at' => 'Updated'
	];

	private function getClients($data)
	{
		$query = 'query {clients {';
		foreach ($data as $key) {
			switch ($key) {
				case 'contacts':
					$query .= ' contacts{name}';
					break;
				
				case 'supports':
					$query .= ' supports{title}';
					break;
				
				case 'meetings':
					$query .= ' meetings{title}';
					break;
				
				default:
					$query .= ' ' . $key;
					break;
			}
		}
		$query .= ' } }';
		return Http::withHeaders([
			'Content-Type' => 'application/json',
		])->post(env('GRAPHQL'), ['query' => $query])->throw()->json();
	}

	private function getContacts($data)
	{
		$query = 'query {contacts {';
		foreach ($data as $key) {
			switch ($key) {
				case 'client':
					$query .= ' client{name}';
					break;
				
				default:
					$query .= ' ' . $key;
					break;
			}
		}
		$query .= ' } }';
		return Http::withHeaders([
			'Content-Type' => 'application/json',
		])->post(env('GRAPHQL'), ['query' => $query])->throw()->json();
	}

	private function getMeetings($data)
	{
		$query = 'query {meetings {';
		foreach ($data as $key) {
			switch ($key) {
				case 'client':
					$query .= ' client{name}';
					break;
				
				case 'users':
					$query .= ' users{username}';
					break;
				
				default:
					$query .= ' ' . $key;
					break;
			}
		}
		$query .= ' } }';
		return Http::withHeaders([
			'Content-Type' => 'application/json',
		])->post(env('GRAPHQL'), ['query' => $query])->throw()->json();
	}

	private function getSupports($data)
	{
		$query = 'query {supports {';
		foreach ($data as $key) {
			switch ($key) {
				case 'client':
					$query .= ' client{name}';
					break;
				
				default:
					$query .= ' ' . $key;
					break;
			}
		}
		$query .= ' } }';
		return Http::withHeaders([
			'Content-Type' => 'application/json',
		])->post(env('GRAPHQL'), ['query' => $query])->throw()->json();
	}




  public function report(Request $request)
  {
  	$validator = Validator::make($request->all(), [
        'data' => 'required|array',
  			'report' => 'required|in:clients,contacts,meetings,supports'
    ], [
    	'data.required' => 'To generate a report, please select at least one option first',
    	'data.array' => 'Please don\'t modify the website',
    	'report.required' => 'Please don\'t modify the website',
    	'report.in' => 'Please don\'t modify the website'
    ]);
    if ($validator->fails()) {
        return view('home.index')->with([
        	'report' => null
      ])->withErrors($validator->errors());
    }
  	/*$request->validate([
  		'data' => 'required|array',
  		'report' => 'required|in:clients,contacts,meetings,supports'
  	], [
  		'data.required' => 'Please select a checkbox first'
  	]);*/
  	$data = $request['data'];
  	switch ($request['report']) {
  		case 'clients':
  			$report = $this->getClients($data);
  			if (array_key_exists('data', $report)) {
  				return view('home.index')->with([
	  				'report' => $report['data']['clients'],
	  				'th' => $this->clients
	  			]);
  			} else {
  				return view('home.index')->with([
  					'report' => null
  				])->withErrors(['Something went wrong getting your report']);
  			}
  			break;
  		case 'contacts':
  			$report = $this->getContacts($data);
  			if (array_key_exists('data', $report)) {
  				return view('home.index')->with([
	  				'report' => $report['data']['contacts'],
	  				'th' => $this->contacts
	  			]);
  			} else {
  				return view('home.index')->with([
  					'report' => null
  				])->withErrors(['Something went wrong getting your report']);
  			}
  			break;
  		case 'meetings':
  			$report = $this->getMeetings($data);
  			if (array_key_exists('data', $report)) {
  				return view('home.index')->with([
	  				'report' => $report['data']['meetings'],
	  				'th' => $this->meetings
	  			]);
  			} else {
  				return view('home.index')->with([
  					'report' => null
  				])->withErrors(['Something went wrong getting your report']);
  			}
  			break;
  		case 'supports':
  			$report = $this->getSupports($data);
  			if (array_key_exists('data', $report)) {
  				return view('home.index')->with([
	  				'report' => $report['data']['supports'],
	  				'th' => $this->supports
	  			]);
  			} else {
  				return view('home.index')->with([
  					'report' => null
  				])->withErrors(['Something went wrong getting your report']);
  			}
  			break;
  	}
  	return view('home.index')->with(['We couldn\'t find information about ' . $request['report']]);
  }
}
