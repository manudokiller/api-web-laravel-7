<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SupportController extends Controller
{
    protected $status = [
        'abierto' => 'Abierto',
        'en_proceso' => 'En Proceso',
        'en_espera' => 'En Espera',
        'finalizado' => 'Finalizado'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supports = $this->get(env('API_SUPPORT'));
        if(!array_key_exists('supports', $supports)){
          return view('support.index')->withErrors($supports['errors']);
        }
        if (empty($supports['supports'])) {
            return view('support.index')->with([
                'supports' => null, 
                'status' => $this->status
            ])->withErrors(['errors' => ['No supports in database']]);
        }
        return view('support.index')->with([
            'supports' => $supports['supports'], 
            'status' => $this->status,
            'clients' => $supports['clients']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
            return view('support.create')->with(['status' => $this->status])->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('support.create')->with(['status' => $this->status])->withErrors(['No clients in database']);
        }
        return view('support.create')->with([
            'clients' => $clients['clients'], 
            'status' => $this->status
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'title' => 'required|max:255',
            'detail' => 'required|max:255',
            'reporter' => 'required|max:255',
            'status' => 'required|max:255|in:abierto,en_proceso,en_espera,finalizado'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $support = $this->post(env('API_SUPPORT'), $request->all());
        if(!array_key_exists('support', $support)){
            $clients = $this->get(env('API_CLIENT'));
            if(!array_key_exists('clients', $clients)){
                return Redirect::back()->withErrors($support['errors'].push($clients['errors']));
            }
            if (empty($clients['clients'])) {
                return Redirect::back()->withErrors($support['errors'].push('No clients in database'));
            }
            return Redirect::back()->withErrors($support['errors']);
        }
        return Redirect()->route('support.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $support = $this->get(env('API_SUPPORT') . '/' . $id);
        if(!array_key_exists('support', $support)){
          return Redirect::back()->withErrors($support['errors']);
        }
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
            return view('support.delete')->with(['status' => $this->status])->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('support.delete')->with(['status' => $this->status])->withErrors(['errors' => ['No clients in database']]);
        }
        return view('support.delete')->with([
            'support' => $support['support'], 
            'clients' => $clients['clients'],
            'status' => $this->status
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $support = $this->get(env('API_SUPPORT') . '/' . $id);
        if(!array_key_exists('support', $support)){
          return Redirect::back()->withErrors($support['errors']);
        }
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
            return view('support.update')->with(['status' => $this->status])->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('support.update')->with(['status' => $this->status])->withErrors(['errors' => ['No clients in database']]);
        }
        return view('support.update')->with([
            'support' => $support['support'], 
            'clients' => $clients['clients'],
            'status' => $this->status
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'title' => 'required|max:255',
            'detail' => 'required|max:255',
            'reporter' => 'required|max:255',
            'status' => 'required|max:255|in:abierto,en_proceso,en_espera,finalizado'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $support = $this->put(env('API_SUPPORT') . '/' . $id, $request->all());
        if(!array_key_exists('support', $support)){
            $clients = $this->get(env('API_CLIENT'));
            if(!array_key_exists('clients', $clients)){
                return Redirect::back()->withErrors($support['errors'].push($clients['errors']));
            }
            if (empty($clients['clients'])) {
                return Redirect::back()->withErrors($support['errors'].push('No clients in database'));
            }
            return Redirect::back()->withErrors($support['errors']);
        }
        return Redirect()->route('support.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $support = $this->delete(env('API_SUPPORT') . '/' . $id);
        if(!array_key_exists('support', $support)){
          return Redirect::back()->withErrors($support['errors']);
        }
        return Redirect()->route('support.index');
    }
}
