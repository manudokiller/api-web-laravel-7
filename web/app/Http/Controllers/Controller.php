<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Http;
use Auth;

class Controller extends BaseController
{
  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function auth_login($url, $parameters)
  {
    return Http::asForm()->post($url, $parameters)->throw()->json();
  }

  public function getWithoutThrow($url)
  {
    return Http::withToken(session('jwt'))->get($url);
  }

  public function get($url)
  {
    return Http::withToken(session('jwt'))->get($url)->throw()->json();
  }

  public function post($url, $parameters)
  {
    return Http::withToken(session('jwt'))->asForm()->post($url, $parameters)->throw()->json();
  }

  public function put($url, $parameters)
  {
    return Http::withToken(session('jwt'))->asForm()->put($url, $parameters)->throw()->json();
  }

  public function delete($url)
  {
    return Http::withToken(session('jwt'))->delete($url)->throw()->json();
  }
}
