<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $meetings = $this->get(env('API_MEETING'));
        if(!array_key_exists('meetings', $meetings)){
          return view('meeting.index')->withErrors($meetings['errors']);
        }
        if (empty($meetings['meetings'])) {
            return view('meeting.index')->with(['meetings' => null])->withErrors(['errors' => ['No meetings in database']]);
        }
        return view('meeting.index')->with([
            'meetings' => $meetings['meetings'],
            'meetings_users' => $meetings['meetings_users'],
            'clients' => $meetings['clients']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
            return view('meeting.create')->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('meeting.create')->withErrors(['No clients in database']);
        }
        $users = $this->get(env('API_USER'));
        if(!array_key_exists('users', $users)){
            return view('meeting.create')->withErrors($users['errors']);
        }
        if (empty($users['users'])) {
            return view('meeting.create')->withErrors(['No users in database']);
        }
        return view('meeting.create')->with([
            'clients' => $clients['clients'],
            'users' => $users['users']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'date' => 'required|date|after:today',
            'time' => 'required|date_format:H:i',
            'virtual' => 'in:on,null',
            'user_ids' => 'required|array'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $meeting = $this->post(env('API_MEETING'), $request->all());
        if(!array_key_exists('meeting', $meeting)){
            return Redirect::back()->withErrors($meeting['errors']);
        }
        return Redirect()->route('meeting.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $meeting = $this->get(env('API_MEETING') . '/' . $id);
      if(!array_key_exists('meeting', $meeting)){
        return Redirect::back()->withErrors($meeting['errors']);
      }
      $clients = $this->get(env('API_CLIENT'));
      if(!array_key_exists('clients', $clients)){
          return view('meeting.update')->withErrors($clients['errors']);
      }
      if (empty($clients['clients'])) {
          return view('meeting.update')->withErrors(['errors' => ['No clients in database']]);
      }
      $users = $this->get(env('API_USER'));
      if(!array_key_exists('users', $users)){
          return view('meeting.update')->withErrors($users['errors']);
      }
      if (empty($users['users'])) {
          return view('meeting.update')->withErrors(['No users in database']);
      }
      return view('meeting.delete')->with([
          'meeting' => $meeting['meeting'],
          'meeting_users' => $meeting['meeting_users'],
          'clients' => $clients['clients'],
          'users' => $users['users']
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $meeting = $this->get(env('API_MEETING') . '/' . $id);
      if(!array_key_exists('meeting', $meeting)){
        return Redirect::back()->withErrors($meeting['errors']);
      }
      $clients = $this->get(env('API_CLIENT'));
      if(!array_key_exists('clients', $clients)){
          return view('meeting.update')->withErrors($clients['errors']);
      }
      if (empty($clients['clients'])) {
          return view('meeting.update')->withErrors(['errors' => ['No clients in database']]);
      }
      $users = $this->get(env('API_USER'));
      if(!array_key_exists('users', $users)){
          return view('meeting.update')->withErrors($users['errors']);
      }
      if (empty($users['users'])) {
          return view('meeting.update')->withErrors(['No users in database']);
      }
      return view('meeting.update')->with([
          'meeting' => $meeting['meeting'],
          'meeting_users' => $meeting['meeting_users'],
          'clients' => $clients['clients'],
          'users' => $users['users']
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'date' => 'required|date',
            'time' => 'required|date_format:H:i',
            'virtual' => 'in:on,null',
            'user_ids' => 'required|array'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $meeting = $this->put(env('API_MEETING') . '/' . $id, $request->all());
        if(!array_key_exists('meeting', $meeting)){
            $clients = $this->get(env('API_CLIENT'));
            if(!array_key_exists('clients', $clients)){
                return Redirect::back()->withErrors($meeting['errors'].push($clients['errors']));
            }
            if (empty($clients['clients'])) {
                return Redirect::back()->withErrors($meeting['errors'].push('No clients in database'));
            }
            return Redirect::back()->withErrors($meeting['errors']);
        }
        return Redirect()->route('meeting.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meeting = $this->delete(env('API_MEETING') . '/' . $id);
        if(!array_key_exists('meeting', $meeting)){
          return Redirect::back()->withErrors($meeting['errors']);
        }
        return Redirect()->route('meeting.index');
    }
}
