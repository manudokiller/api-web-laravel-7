<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = $this->get(env('API_CONTACT'));
        if(!array_key_exists('contacts', $contacts)){
          return view('contact.index')->withErrors($contacts['errors']);
        }
        if (empty($contacts['contacts'])) {
            return view('contact.index')->with(['contacts' => null])->withErrors(['errors' => ['No contacts in database']]);
        }
        return view('contact.index')->with([
            'contacts' => $contacts['contacts'],
            'clients' => $contacts['clients']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
            return view('contact.create')->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('contact.create')->withErrors(['No clients in database']);
        }
        return view('contact.create')->with(['clients' => $clients['clients']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'email' => 'required|max:255|email',
            'phone_number' => 'required|numeric|digits:8',
            'job' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $contact = $this->post(env('API_CONTACT'), $request->all());
        if(!array_key_exists('contact', $contact)){
            $clients = $this->get(env('API_CLIENT'));
            if(!array_key_exists('clients', $clients)){
                return Redirect::back()->withErrors($contact['errors'].push($clients['errors']));
            }
            if (empty($clients['clients'])) {
                return Redirect::back()->withErrors($contact['errors'].push('No clients in database'));
            }
            return Redirect::back()->withErrors($contact['errors']);
        }
        return Redirect()->route('contact.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contact = $this->get(env('API_CONTACT') . '/' . $id);
        if(!array_key_exists('contact', $contact)){
          return Redirect::back()->withErrors($contact['errors']);
        }
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
            return view('contact.delete')->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('contact.delete')->withErrors(['errors' => ['No clients in database']]);
        }
        return view('contact.delete')->with([
            'contact' => $contact['contact'], 
            'clients' => $clients['clients']
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = $this->get(env('API_CONTACT') . '/' . $id);
        if(!array_key_exists('contact', $contact)){
          return Redirect::back()->withErrors($contact['errors']);
        }
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
            return view('contact.update')->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('contact.update')->withErrors(['errors' => ['No clients in database']]);
        }
        return view('contact.update')->with([
            'contact' => $contact['contact'], 
            'clients' => $clients['clients']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'client_id' => 'required',
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'email' => 'required|max:255|email',
            'phone_number' => 'required|numeric|digits:8',
            'job' => 'required|max:255'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $contact = $this->put(env('API_CONTACT') . '/' . $id, $request->all());
        if(!array_key_exists('contact', $contact)){
            $clients = $this->get(env('API_CLIENT'));
            if(!array_key_exists('clients', $clients)){
                return Redirect::back()->withErrors($contact['errors'].push($clients['errors']));
            }
            if (empty($clients['clients'])) {
                return Redirect::back()->withErrors($contact['errors'].push('No clients in database'));
            }
            return Redirect::back()->withErrors($contact['errors']);
        }
        return Redirect()->route('contact.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = $this->delete(env('API_CONTACT') . '/' . $id);
        if(!array_key_exists('contact', $contact)){
          return Redirect::back()->withErrors($contact['errors']);
        }
        return Redirect()->route('contact.index');
    }
}
