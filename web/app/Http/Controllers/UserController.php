<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->get(env('API_USER'));
        if(!array_key_exists('users', $users)){
          return view('user.index')->withErrors($users['errors']);
        }
        if (empty($users['users'])) {
            return view('user.index')->with(['users' => null])->withErrors(['errors' => ['No users in database']]);
        }
        return view('user.index')->with(['users' => $users['users'], 'meetings' => $users['meetings']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'username' => 'required|max:255',
            'password' => 'required|max:255|min:8',
            'admin' => 'required|boolean',
        ]);
        $user = $this->post(env('API_USER'), $request->all());
        if(!array_key_exists('user', $user)){
          return Redirect::back()->withErrors($user['errors']);
        }
        return Redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->get(env('API_USER') . '/' . $id);
        if(!array_key_exists('user', $user)){
          return Redirect::back()->withErrors($user['errors']);
        }
        return view('user.delete')->with('user', $user['user']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->get(env('API_USER') . '/' . $id);
        if(!array_key_exists('user', $user)){
          return Redirect::back()->withErrors($user['errors']);
        }
        return view('user.update')->with('user', $user['user']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'username' => 'required|max:255',
            'password' => 'required|max:255|min:8',
            'admin' => 'required|boolean',
        ]);
        $user = $this->put(env('API_USER') . '/' . $id, $request->all());
        if(!array_key_exists('user', $user)){
          return Redirect::back()->withErrors($user['errors']);
        }
        return Redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->delete(env('API_USER') . '/' . $id);
        if(!array_key_exists('user', $user)){
          return Redirect::back()->withErrors($user['errors']);
        }
        return Redirect()->route('user.index');
    }
}
