<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    protected $sectors = [
        'educacion' => 'Educación',
        'industria' => 'Industria',
        'agricultura' => 'Agricultura',
        'manufactura' => 'Manufactura',
        'servicios' => 'Servicios',
        'otros' => 'Otros'
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->get(env('API_CLIENT'));
        if(!array_key_exists('clients', $clients)){
          return view('client.index')->withErrors($clients['errors']);
        }
        if (empty($clients['clients'])) {
            return view('client.index')->with([
                'clients' => null, 
                'sectors' => $this->sectors
            ])->withErrors(['errors' => ['No clients in database']]);
        }
        return view('client.index')->with([
            'clients' => $clients['clients'], 
            'sectors' => $this->sectors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create')->with(['sectors' => $this->sectors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'legal_certificate' => 'required|numeric|digits:10',
            'website' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => 'required|numeric|digits:8',
            'sector' => 'required|in:educacion,industria,agricultura,manufactura,servicios,otros'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $client = $this->post(env('API_CLIENT'), $request->all());

        if(!array_key_exists('client', $client)){
          return Redirect::back()->withErrors($client['errors']);
        }
        return Redirect()->route('client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = $this->get(env('API_CLIENT') . '/' . $id);
        if(!array_key_exists('client', $client)){
          return Redirect::back()->withErrors($client['errors']);
        }
        return view('client.delete')->with([
            'client' => $client['client'], 
            'sectors' => $this->sectors
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = $this->get(env('API_CLIENT') . '/' . $id);
        if(!array_key_exists('client', $client)){
          return Redirect::back()->withErrors($client['errors']);
        }
        return view('client.update')->with([
            'client' => $client['client'], 
            'sectors' => $this->sectors
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'legal_certificate' => 'required|digits:10',
            'website' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => 'required|digits:8',
            'sector' => 'required|in:educacion,industria,agricultura,manufactura,servicios,otros'
        ]);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator->errors());
        }
        $client = $this->put(env('API_CLIENT') . '/' . $id, $request->all());
        if(!array_key_exists('client', $client)){
          return Redirect::back()->withErrors($client['errors']);
        }
        return Redirect()->route('client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = $this->delete(env('API_CLIENT') . '/' . $id);
        if(!array_key_exists('client', $client)){
          return Redirect::back()->withErrors($client['errors']);
        }
        return Redirect()->route('client.index');
    }
}
