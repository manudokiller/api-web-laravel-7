<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeetingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'client_id' => 'exists:clients,id',
            'title' => 'required|max:255',
            'date_time' => 'required|date',
            'virtual' => 'required|boolean',
            'users' => 'required|array|exists:users,id'
        ];
    }
}
