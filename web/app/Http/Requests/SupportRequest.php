<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupportRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'client_id' => 'required|exists:clients,id',
            'title' => 'required|max:255',
            'detail' => 'required|max:255',
            'reporter' => 'required|max:255',
            'status' => 'required|in:abierto,"en proceso","en espera",finalizado'
        ];
    }
}
