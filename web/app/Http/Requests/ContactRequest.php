<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'client_id' => 'required|exists:clients,id',
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'email' => 'required|email|unique:contacts,email',
            'phone_number' => 'required|digits:8',
            'job' => 'required|max:255'
        ];
    }
}
