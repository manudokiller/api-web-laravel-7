<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:clients,name',
            'legal_certificate' => 'required|digits:10|unique:clients,name',
            'website' => 'required|max:255',
            'address' => 'required|max:255',
            'phone_number' => 'required|digits:8',
            'sector' => 'required|in:educacion,industria,agricultura,manufactura,servicios,otros'
        ];
    }
}
