<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'lastnames' => 'required|max:255',
            'username' => 'required|max:255|unique:users,username',
            'password' => 'required|max:255|min:8'
        ];
    }
}
