@extends('layouts.app')

@section('style')
    <style type="text/css">
        a .fas {
            color: black;
        }
        a .fas:hover {
            transform: scale(1.5);
        }
        .right {
            float: right;
        }
        .row {
            margin-bottom: 5px;
        }
    </style>
@endsection
    
@section('content')
    
    
    <div class="row">
        <div class="col-md-6">
            <h3>Meetings</h3>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary right" href="{{ route('meeting.create') }}">New</a>
        </div>
    </div>

    @if($meetings)
        <table class="table table-hover">
            <thead>
                <th>Client</th>
                <th>Title</th>
                <th>DateTime</th>
                <th>Virtual</th>
                <th>Users</th>
                <th>Options</th>
            </thead>
            <tbody>
                @foreach($meetings as $meeting)
                    <tr>
                        @if($meeting['client_id'])
                            <td>{{ $clients[$meeting['client_id'] . ' '] }}</td>
                        @else
                            <td>Not Available</td>
                        @endif
                        <td>{{ $meeting['title'] }}</td>
                        <td>{{ $meeting['date_time'] }}</td>
                        @if($meeting['virtual'] == 1)
                            <td>Yes</td>
                        @else
                            <td>No</td>
                        @endif
                        @if(in_array($meeting['id'], array_keys($meetings_users)))
                            <td>{{ $meetings_users[$meeting['id'] . ' '] }}</td>
                        @else
                            <td>Miising Users</td>
                        @endif
                        <td>
                            <a href="{{ route('meeting.edit', [$meeting['id']]) }}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                             - 
                            <a href="{{ route('meeting.show', [$meeting['id']]) }}" data-toggle="tooltip" title="Delete"><i class="fas fa-eraser"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@endsection