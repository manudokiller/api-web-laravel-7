@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Create a new Meeting</h3>
    </div>
    <div class="card-body">
      {!! Form::open(['route' => 'meeting.store', 'method' => 'post']) !!}
        <div class="form-group">
          {!! Form::label('client_id', 'Client:', ['class' => 'control-label']) !!}
          <select class="form-control" name="client_id">
            @if($clients)
              <option value='null'>-- Click to select a client --</option>
              @foreach($clients as $client)
                <option value="{{ $client['id'] }}">{{ $client['name'] }}</option>
              @endforeach
            @else
              <option value='null'>-- No Clients Available --</option>
            @endif            
          </select>
        </div>
        <div class="form-group">
          {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
          {!! Form::text('title', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('date', 'Date:', ['class' => 'control-label']) !!}
          {!! Form::date('date', \Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'required' => 'true', 'min' => \Carbon\Carbon::now()->format('Y-m-d')]) !!}
        </div>
        <div class="form-group">
          {!! Form::label('time', 'Time:', ['class' => 'control-label']) !!}
          {!! Form::time('time', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          <div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="virtual" name="virtual">
            {!! Form::label('virtual', 'Virtual', ['class' => 'custom-control-label']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('user_ids', 'Users:', ['class' => 'control-label']) !!}
          <select class="custom-select" name="user_ids[]" size="4" multiple>
            @if($users)
              @foreach($users as $user)
                <option value="{{ $user['id'] }}">{{ $user['username'] }}</option>
              @endforeach
            @else
              <option value='0'>-- No Users Available --</option>
            @endif            
          </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('meeting.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
    
@endsection