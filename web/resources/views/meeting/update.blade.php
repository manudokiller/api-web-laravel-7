@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')
    
  <div class="card">
    <div class="card-header">
      <h3>Updating Meeting: #{{ $meeting['id'] }}</h3>
    </div>
    <div class="card-body">
      {!! Form::model($meeting, ['route' => ['meeting.update', $meeting['id']], 'method' => 'put']) !!}
        <div class="form-group">
          {!! Form::label('client_id', 'Client:', ['class' => 'control-label']) !!}
          <select class="form-control" name="client_id">
            @if($clients)
              <option value='null'>-- Click to select a client --</option>
              @foreach($clients as $client)
                @if($client['id'] == $meeting['client_id'])
                  <option value="{{ $client['id'] }}" selected>{{ $client['name'] }}</option>
                @else
                  <option value="{{ $client['id'] }}">{{ $client['name'] }}</option>
                @endif
              @endforeach
            @else
              <option value='null'>-- No Clients Available --</option>
            @endif            
          </select>
        </div>
        <div class="form-group">
          {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
          {!! Form::text('title', $meeting['title'] , ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('date', 'Date:', ['class' => 'control-label']) !!}
          {!! Form::date('date', date('Y-m-d', strtotime($meeting['date_time'])), ['class' => 'form-control', 'required' => 'true', 'min' => \Carbon\Carbon::now()->format('Y-m-d')]) !!}
        </div>
        <div class="form-group">
          {!! Form::label('time', 'Time:', ['class' => 'control-label']) !!}
          {!! Form::time('time', date('H:i', strtotime($meeting['date_time'])), ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          <div class="custom-control custom-switch">
            @if($meeting['virtual'])
              <input type="checkbox" class="custom-control-input" id="virtual" name="virtual" checked>
            @else
              <input type="checkbox" class="custom-control-input" id="virtual" name="virtual">
            @endif
            {!! Form::label('virtual', 'Virtual', ['class' => 'custom-control-label']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('user_ids', 'Users:', ['class' => 'control-label']) !!}
          <select class="custom-select" name="user_ids[]" size="4" multiple>
            @if($users)
              @if($meeting_users)
                @foreach($users as $user)
                  @if(in_array($user['username'], explode(' - ', $meeting_users)))
                    <option value="{{ $user['id'] }}" selected>{{ $user['username'] }}</option>
                  @else
                    <option value="{{ $user['id'] }}">{{ $user['username'] }}</option>
                  @endif
                @endforeach
              @else
                @foreach($users as $user)
                  <option value="{{ $user['id'] }}">{{ $user['username'] }}</option>
                @endforeach
              @endif
            @else
              <option value='0'>-- No Users Available --</option>
            @endif            
          </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('meeting.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>

@endsection