@extends('layouts.app')

@section('style')
    <style type="text/css">
        a .fas {
            color: black;
        }
        a .fas:hover {
            transform: scale(1.5);
        }
        .right {
            float: right;
        }
        .row {
            margin-bottom: 5px;
        }
    </style>
@endsection
    
@section('content')
    
    
    <div class="row">
        <div class="col-md-6">
            <h3>Clients</h3>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary right" href="{{ route('client.create') }}">New</a>
        </div>
    </div>

    @if($clients)
        <table class="table table-hover">
            <thead>
                <th>Name</th>
                <th>Legal Certificate</th>
                <th>Website</th>
                <th>Address</th>
                <th>Phone Number</th>
                <th>Sector</th>
                <th>Options</th>
            </thead>
            <tbody>
                @foreach($clients as $client)
                    <tr>
                        <td>{{ $client['name'] }}</td>
                        <td>{{ $client['legal_certificate'] }}</td>
                        <td>{{ $client['website'] }}</td>
                        <td>{{ $client['address'] }}</td>
                        <td>{{ $client['phone_number'] }}</td>
                        <td>{{ $sectors[$client['sector']] }}</td>
                        <td>
                            <a href="{{ route('client.edit', [$client['id']]) }}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                             - 
                            <a href="{{ route('client.show', [$client['id']]) }}" data-toggle="tooltip" title="Delete"><i class="fas fa-eraser"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@endsection