@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Create a new Client</h3>
    </div>
    <div class="card-body">
      {!! Form::open(['route' => 'client.store', 'method' => 'post']) !!}
        <div class="form-group">
          {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
          {!! Form::text('name', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('legal_certificate', 'Legal Certificate:', ['class' => 'control-label']) !!}
          {!! Form::number('legal_certificate', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('website', 'Website:', ['class' => 'control-label']) !!}
          {!! Form::text('website', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
          {!! Form::text('address', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('phone_number', 'Phone Number:', ['class' => 'control-label']) !!}
          {!! Form::number('phone_number', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('sector', 'Sector:', ['class' => 'control-label']) !!}
          <select class="form-control" name="sector">
            @foreach(array_keys($sectors) as $sector_key)
              <option value='{{ $sector_key }}'>{{ $sectors[$sector_key] }}</option>
            @endforeach
            </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('client.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
    
@endsection