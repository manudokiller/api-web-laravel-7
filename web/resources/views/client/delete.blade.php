@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Deleting Client: {{ $client['name'] }}</h3>
    </div>
    <div class="card-body">
      {!! Form::model($client, ['route' => ['client.destroy', $client['id']], 'method' => 'delete']) !!}
        <div class="form-group">
          {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
          {!! Form::text('name', $client['name'], ['class' => 'form-control', 'disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('legal_certificate', 'Legal Certificate:', ['class' => 'control-label']) !!}
          {!! Form::number('legal_certificate', $client['legal_certificate'], ['class' => 'form-control', 'disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('website', 'Website:', ['class' => 'control-label']) !!}
          {!! Form::text('website', $client['website'], ['class' => 'form-control', 'disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('address', 'Address:', ['class' => 'control-label']) !!}
          {!! Form::text('address', $client['address'], ['class' => 'form-control', 'disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('phone_number', 'Phone Number:', ['class' => 'control-label']) !!}
          {!! Form::number('phone_number', $client['phone_number'], ['class' => 'form-control', 'disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('sector', 'Sector:', ['class' => 'control-label', 'disabled']) !!}
          <select class="form-control" name="sector" disabled>
            @foreach(array_keys($sectors) as $sector_key)
              @if($sector_key == $client['sector'])
                <option value='{{ $sector_key }}' selected>{{ $sectors[$sector_key] }}</option>
              @else
                <option value='{{ $sector_key }}'>{{ $sectors[$sector_key] }}</option>
              @endif
            @endforeach
          </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        <a href="{{ route('client.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>

@endsection