<div class="modal fade" id="reports_modal" tabindex="-1" role="dialog" aria-labelledby="reports_modal_title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="reports_modal_title">Generate a Report</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 offset-md-1">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-client-tab" data-toggle="pill" href="#v-pills-client" role="tab" aria-controls="v-pills-client" aria-selected="true">Client</a>
                            <a class="nav-link" id="v-pills-contact-tab" data-toggle="pill" href="#v-pills-contact" role="tab" aria-controls="v-pills-contact" aria-selected="false">Contact</a>
                            <a class="nav-link" id="v-pills-support-tab" data-toggle="pill" href="#v-pills-support" role="tab" aria-controls="v-pills-support" aria-selected="false">Support Ticket</a>
                            <a class="nav-link" id="v-pills-meeting-tab" data-toggle="pill" href="#v-pills-meeting" role="tab" aria-controls="v-pills-meeting" aria-selected="false">Meeting</a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="tab-content" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-client" role="tabpanel" aria-labelledby="v-pills-client-tab">
                                {!! Form::open(['route' => 'report', 'method' => 'post']) !!}
                                    {!! Form::token() !!}
                                    <input type="hidden" name="report" value="clients">
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'name') }}
                                        {{ Form::label('data', 'Name', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'legal_certificate') }}
                                        {{ Form::label('data', 'Legal Certificate', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'website') }}
                                        {{ Form::label('data', 'Website', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'address') }}
                                        {{ Form::label('data', 'Address', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'phone_number') }}
                                        {{ Form::label('data', 'Phone Number', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'sector') }}
                                        {{ Form::label('data', 'Sector', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'contacts') }}
                                        {{ Form::label('data', 'Contacts (name)', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'supports') }}
                                        {{ Form::label('data', 'Support Tickets (title)', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'meetings') }}
                                        {{ Form::label('data', 'Meetings (title)', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'created_at') }}
                                        {{ Form::label('data', 'Created at', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'updated_at') }}
                                        {{ Form::label('data', 'Updated at', ['class' => 'control-label']) }}
                                    </div>
                                    {!! Form::submit('Search', ['class' => 'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade" id="v-pills-contact" role="tabpanel" aria-labelledby="v-pills-contact-tab">
                                {!! Form::open(['route' => 'report', 'method' => 'post']) !!}
                                    {!! Form::token() !!}
                                    <input type="hidden" name="report" value="contacts">
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'client') }}
                                        {{ Form::label('data', 'Client (name)', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'name') }}
                                        {{ Form::label('data', 'Name', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'lastnames') }}
                                        {{ Form::label('data', 'Lastnames', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'email') }}
                                        {{ Form::label('data', 'Email', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'phone_number') }}
                                        {{ Form::label('data', 'Phone Number', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'job') }}
                                        {{ Form::label('data', 'Job', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'created_at') }}
                                        {{ Form::label('data', 'Created at', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'updated_at') }}
                                        {{ Form::label('data', 'Updated at', ['class' => 'control-label']) }}
                                    </div>
                                    {!! Form::submit('Search', ['class' => 'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade" id="v-pills-support" role="tabpanel" aria-labelledby="v-pills-support-tab">
                                {!! Form::open(['route' => 'report', 'method' => 'post']) !!}
                                    {!! Form::token() !!}
                                    <input type="hidden" name="report" value="supports">
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'client') }}
                                        {{ Form::label('data', 'Client (name)', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'title') }}
                                        {{ Form::label('data', 'Title', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'detail') }}
                                        {{ Form::label('data', 'Detail', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'reporter') }}
                                        {{ Form::label('data', 'Reporter', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'status') }}
                                        {{ Form::label('data', 'Status', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'created_at') }}
                                        {{ Form::label('data', 'Created at', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'updated_at') }}
                                        {{ Form::label('data', 'Updated at', ['class' => 'control-label']) }}
                                    </div>
                                    {!! Form::submit('Search', ['class' => 'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                            <div class="tab-pane fade" id="v-pills-meeting" role="tabpanel" aria-labelledby="v-pills-meeting-tab">
                                {!! Form::open(['route' => 'report', 'method' => 'post']) !!}
                                    {!! Form::token() !!}
                                    <input type="hidden" name="report" value="meetings">
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'client') }}
                                        {{ Form::label('data', 'Client (name)', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'title') }}
                                        {{ Form::label('data', 'Title', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'date_time') }}
                                        {{ Form::label('data', 'Datetime', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'virtual') }}
                                        {{ Form::label('data', 'Virtual', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'users') }}
                                        {{ Form::label('data', 'Users (username)', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'created_at') }}
                                        {{ Form::label('data', 'Created at', ['class' => 'control-label']) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::checkbox('data[]', 'updated_at') }}
                                        {{ Form::label('data', 'Updated at', ['class' => 'control-label']) }}
                                    </div>
                                    {!! Form::submit('Search', ['class' => 'btn btn-success']) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>