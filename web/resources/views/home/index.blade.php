@extends('layouts.app')

@section('style')
    <style>
        h1, h3 {
            text-align: center;
        }
        .row {
            margin-bottom: 10px;
        }
        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
            margin-bottom: 30px;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
            position: relative;
            height: 80vh;
        }
    </style>
@endsection
    
@section('content')

    @if($report)
        <h3>{{ $th['report_title'] }} Report</h3>
        <table class="table table-hover">
            <thead>
                @foreach(array_keys($report[0]) as $key)
                    <th>{{ $th[$key] }}</th>
                @endforeach
            </thead>
            <tbody>
                @foreach($report as $report_object)
                    <tr>
                        @foreach(array_keys($report[0]) as $value)
                            @if(is_array($report_object[$value]))
                                <td>
                                    <?php $count = 1; ?>
                                    @foreach($report_object[$value] as $val)
                                        @if(is_array($val))
                                            ({{ $count }})-{{ $val[array_keys($val)[0]] }} <br>
                                        @else
                                            {{ $val }}
                                        @endif
                                        <?php $count++; ?>
                                    @endforeach
                                </td>
                            @else
                                @if($report_object[$value] == '1')
                                    <td>Yes</td>
                                @elseif($report_object[$value] == '0')
                                    <td>No</td>
                                @elseif(!$report_object[$value])
                                    <td>Not Available</td>
                                @else
                                    <td>{{ $report_object[$value] }}</td>
                                @endif
                            @endif
                            
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="flex-center">
            <div class="content">
                <div class="title">
                    Welcome
                </div>
            </div>
        </div>
    @endif

@endsection