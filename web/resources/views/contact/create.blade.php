@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Create a new Contact</h3>
    </div>
    <div class="card-body">
      {!! Form::open(['route' => 'contact.store', 'method' => 'post']) !!}
        <div class="form-group">
          {!! Form::label('client_id', 'Contact:', ['class' => 'control-label']) !!}
          <select class="form-control" name="client_id">
            @if($clients)
              @foreach($clients as $client)
                <option value="{{ $client['id'] }}">{{ $client['name'] }}</option>
              @endforeach
            @else
              <option value='0'>-- No Clients Available --</option>
            @endif            
          </select>
        </div>
        <div class="form-group">
          {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
          {!! Form::text('name', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('lastnames', 'Surnames:', ['class' => 'control-label']) !!}
          {!! Form::text('lastnames', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
          {!! Form::email('email', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('phone_number', 'Phone Number:', ['class' => 'control-label']) !!}
          {!! Form::number('phone_number', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('job', 'Job:', ['class' => 'control-label']) !!}
          {!! Form::text('job', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('contact.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
    
@endsection