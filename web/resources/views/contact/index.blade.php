@extends('layouts.app')

@section('style')
    <style type="text/css">
        a .fas {
            color: black;
        }
        a .fas:hover {
            transform: scale(1.5);
        }
        .right {
            float: right;
        }
        .row {
            margin-bottom: 5px;
        }
    </style>
@endsection
    
@section('content')
    
    
    <div class="row">
        <div class="col-md-6">
            <h3>Contacts</h3>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary right" href="{{ route('contact.create') }}">New</a>
        </div>
    </div>

    @if($contacts)
        <table class="table table-hover">
            <thead>
                <th>Client</th>
                <th>Name</th>
                <th>Surnames</th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>Job</th>
                <th>Options</th>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                    <tr>
                        <td>{{ $clients[$contact['client_id'] . ' '] }}</td>
                        <td>{{ $contact['name'] }}</td>
                        <td>{{ $contact['lastnames'] }}</td>
                        <td>{{ $contact['email'] }}</td>
                        <td>{{ $contact['phone_number'] }}</td>
                        <td>{{ $contact['job'] }}</td>
                        <td>
                            <a href="{{ route('contact.edit', [$contact['id']]) }}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                             - 
                            <a href="{{ route('contact.show', [$contact['id']]) }}" data-toggle="tooltip" title="Delete"><i class="fas fa-eraser"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@endsection