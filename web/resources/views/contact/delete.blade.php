@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Deleting contact: {{ $contact['name'] }}</h3>
    </div>
    <div class="card-body">
      {!! Form::model($contact, ['route' => ['contact.destroy', $contact['id']], 'method' => 'delete']) !!}
        <div class="form-group">
          {!! Form::label('client_id', 'Client:', ['class' => 'control-label']) !!}
          <select class="form-control" name="client_id" disabled>
            @if($clients)
              @foreach($clients as $client)
                @if($client['id'] == $contact['client_id'])
                  <option value="{{ $client['id'] }}" selected>{{ $client['name'] }}</option>
                @else
                  <option value="{{ $client['id'] }}">{{ $client['name'] }}</option>
                @endif
              @endforeach
            @else
              <option value='0'>-- No Clients Available --</option>
            @endif            
          </select>
        </div>
        <div class="form-group">
          {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
          {!! Form::text('name', $contact['name'], ['class' => 'form-control', ' disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('lastnames', 'Surnames:', ['class' => 'control-label']) !!}
          {!! Form::text('lastnames', $contact['lastnames'], ['class' => 'form-control', ' disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
          {!! Form::email('email', $contact['email'], ['class' => 'form-control', ' disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('phone_number', 'Phone Number:', ['class' => 'control-label']) !!}
          {!! Form::number('phone_number', $contact['phone_number'], ['class' => 'form-control', ' disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('job', 'Job:', ['class' => 'control-label']) !!}
          {!! Form::text('job', $contact['job'], ['class' => 'form-control', ' disabled']) !!}
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        <a href="{{ route('contact.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>

@endsection