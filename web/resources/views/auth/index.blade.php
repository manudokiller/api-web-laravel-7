<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel Web</title>

        <!-- Fonts -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Styles -->
        <style>
            .center {
                margin: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
            }
            .half-size {
                width: 50%;
            }

            h3 {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="card half-size center">
                <div class="card-header">
                    <h3>ISW 711 - Login</h3>
                </div> 
                <div class="card-body">

                    @include('layouts.errors')

                    {!! Form::open(['route' => 'login', 'method' => 'post']) !!}
                        {!! Form::token() !!}
                        <div class="form-group">
                            {{ Form::label('username', 'Username:', ['class' => 'control-label']) }}
                            {{ Form::text('username', null, array_merge(['class' => 'form-control', 'placeholder' => 'Write your username...'])) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('password', 'Password:', ['class' => 'control-label']) }}
                            {{ Form::password('password', ['class' => 'form-control']) }}
                        </div>
                </div>
                <div class="card-footer">
                        {!! Form::submit('Login', ['class' => 'btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </body>
</html>
