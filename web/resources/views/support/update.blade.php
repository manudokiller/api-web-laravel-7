@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')
    
  <div class="card">
    <div class="card-header">
      <h3>Updating Support Ticket: #{{ $support['id'] }}</h3>
    </div>
    <div class="card-body">
      {!! Form::model($support, ['route' => ['support.update', $support['id']], 'method' => 'put']) !!}
        <div class="form-group">
          {!! Form::label('client_id', 'Client:', ['class' => 'control-label']) !!}
          <select class="form-control" name="client_id">
            @if($clients)
              @foreach($clients as $client)
                @if($client['id'] == $support['client_id'])
                  <option value="{{ $client['id'] }}" selected>{{ $client['name'] }}</option>
                @else
                  <option value="{{ $client['id'] }}">{{ $client['name'] }}</option>
                @endif
              @endforeach
            @else
              <option value='0'>-- No Clients Available --</option>
            @endif            
          </select>
        </div>
        <div class="form-group">
          {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
          {!! Form::text('title', $support['title'], ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('detail', 'Detail:', ['class' => 'control-label']) !!}
          {!! Form::text('detail', $support['detail'], ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('reporter', 'Reporter:', ['class' => 'control-label']) !!}
          {!! Form::text('reporter', $support['reporter'], ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('status', 'Status:', ['class' => 'control-label']) !!}
          <select class="form-control" name="status">
            @foreach(array_keys($status) as $status_key)
              @if($status_key == $support['status'])
                <option value='{{ $status_key }}' selected>{{ $status[$status_key] }}</option>
              @else
                <option value='{{ $status_key }}'>{{ $status[$status_key] }}</option>
              @endif
            @endforeach
          </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('support.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>

@endsection