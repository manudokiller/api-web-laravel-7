@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Create a new Support Ticket</h3>
    </div>
    <div class="card-body">
      {!! Form::open(['route' => 'support.store', 'method' => 'post']) !!}
        <div class="form-group">
          {!! Form::label('client_id', 'Client:', ['class' => 'control-label']) !!}
          <select class="form-control" name="client_id">
            @if($clients)
              @foreach($clients as $client)
                <option value="{{ $client['id'] }}">{{ $client['name'] }}</option>
              @endforeach
            @else
              <option value='0'>-- No Clients Available --</option>
            @endif            
          </select>
        </div>
        <div class="form-group">
          {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
          {!! Form::text('title', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('detail', 'Detail:', ['class' => 'control-label']) !!}
          {!! Form::text('detail', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('reporter', 'Reporter:', ['class' => 'control-label']) !!}
          {!! Form::text('reporter', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('status', 'Status:', ['class' => 'control-label']) !!}
          <select class="form-control" name="status">
            @foreach(array_keys($status) as $status_key)
              <option value='{{ $status_key }}'>{{ $status[$status_key] }}</option>
            @endforeach
            </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('support.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
    
@endsection