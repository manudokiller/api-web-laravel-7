@extends('layouts.app')

@section('style')
    <style type="text/css">
        a .fas {
            color: black;
        }
        a .fas:hover {
            transform: scale(1.5);
        }
        .right {
            float: right;
        }
        .row {
            margin-bottom: 5px;
        }
    </style>
@endsection
    
@section('content')
    
    
    <div class="row">
        <div class="col-md-6">
            <h3>Support Tickets</h3>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary right" href="{{ route('support.create') }}">New</a>
        </div>
    </div>

    @if($supports)
        <table class="table table-hover">
            <thead>
                <th>Client</th>
                <th>Title</th>
                <th>Detail</th>
                <th>Reporter</th>
                <th>Status</th>
                <th>Options</th>
            </thead>
            <tbody>
                @foreach($supports as $support)
                    <tr>
                        <td>{{ $clients[$support['client_id'] . ' '] }}</td>
                        <td>{{ $support['title'] }}</td>
                        <td>{{ $support['detail'] }}</td>
                        <td>{{ $support['reporter'] }}</td>
                        <td>{{ $status[$support['status']] }}</td>
                        <td>
                            <a href="{{ route('support.edit', [$support['id']]) }}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                             - 
                            <a href="{{ route('support.show', [$support['id']]) }}" data-toggle="tooltip" title="Delete"><i class="fas fa-eraser"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@endsection