@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Deleting User: {{ $user['username'] }}</h3>
    </div>
    <div class="card-body">
      {!! Form::model($user, ['route' => ['user.destroy', $user['id']], 'method' => 'delete']) !!}
        <input type="hidden">
        <div class="form-group">
          {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
          {!! Form::text('name', $user['name'], ['class' => 'form-control', 'disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('lastnames', 'Surnames:', ['class' => 'control-label']) !!}
          {!! Form::text('lastnames', $user['lastnames'], ['class' => 'form-control', 'disabled']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
           {!! Form::text('username', $user['username'], ['class' => 'form-control', 'disabled']) !!}
          
        </div>
        <div class="form-group">
          {!! Form::label('admin', 'Admin Role:', ['class' => 'control-label']) !!}
          <select class="form-control" name="admin" disabled>
            @if($user['admin'])
              <option value='0'>Not</option>
              <option value='1' selected="true">Yes</option>
            @else
              <option value='0' selected="true">Not</option>
              <option value='1'>Yes</option>
            @endif
          </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        <a href="{{ route('user.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>

@endsection