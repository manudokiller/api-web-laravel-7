@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')
    
  <div class="card">
    <div class="card-header">
      <h3>Updating User: {{ $user['username'] }}</h3>
    </div>
    <div class="card-body">
      {!! Form::model($user, ['route' => ['user.update', $user['id']], 'method' => 'put']) !!}
        <input type="hidden">
        <div class="form-group">
          {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
          {!! Form::text('name', $user['name'], ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('lastnames', 'Surnames:', ['class' => 'control-label']) !!}
          {!! Form::text('lastnames', $user['lastnames'], ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
          @if($user['username'] == 'Admin')
            {!! Form::text('username', $user['username'], ['class' => 'form-control', 'readonly']) !!}
          @else
            {!! Form::text('username', $user['username'], ['class' => 'form-control', 'required' => 'true']) !!}
          @endif
          
        </div>
        <div class="form-group">
          {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
          {!! Form::password('password', ['class' => 'form-control', 'required' => 'true', 'minlength' => '8']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('admin', 'Admin Role:', ['class' => 'control-label']) !!}
          @if($user['username'] == 'Admin')
            <input type="hidden" name="admin" value="1">
            <select class="form-control" disabled>
              <option value='1'>Yes</option>
            </select>
          @else
            <select class="form-control" name="admin">
              @if($user['admin'])
                <option value='0'>Not</option>
                <option value='1' selected="true">Yes</option>
              @else
                <option value='0' selected="true">Not</option>
                <option value='1'>Yes</option>
              @endif
            </select>
          @endif
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Update', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('user.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>

@endsection