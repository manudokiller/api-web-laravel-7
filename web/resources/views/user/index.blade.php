@extends('layouts.app')

@section('style')
    <style type="text/css">
        a .fas {
            color: black;
        }
        a .fas:hover {
            transform: scale(1.5);
        }
        .right {
            float: right;
        }
        .row {
            margin-bottom: 5px;
        }
    </style>
@endsection
    
@section('content')
    
    
    <div class="row">
        <div class="col-md-6">
            <h3>Users</h3>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary right" href="{{ route('user.create') }}">New</a>
        </div>
    </div>

    @if($users)
        <table class="table table-hover">
            <thead>
                <th>Name</th>
                <th>Surnames</th>
                <th>Username</th>
                <th>Admin</th>
                <th>Meetings</th>
                <th>Options</th>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user['name'] }}</td>
                        <td>{{ $user['lastnames'] }}</td>
                        <td>{{ $user['username'] }}</td>
                        @if($user['admin'])
                            <td><i class="fas fa-check-circle"></i></td>
                        @else
                            <td><i class="fas fa-times-circle"></i></td>
                        @endif
                        @if(array_key_exists($user['id'], $meetings))
                            <td>{{ $meetings[$user['id']] }}</td>
                        @else
                            <td>Not Available</td>
                        @endif
                        <td>
                            <a href="{{ route('user.edit', [$user['id']]) }}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                             - 
                            <a href="{{ route('user.show', [$user['id']]) }}" data-toggle="tooltip" title="Delete"><i class="fas fa-eraser"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@endsection