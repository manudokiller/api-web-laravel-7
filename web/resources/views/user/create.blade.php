@extends('layouts.app')

@section('style')
  <style type="text/css">
    .card {
      width: 50%;
      left: 25%;
    }
  </style>
@endsection
    
@section('content')

  <div class="card">
    <div class="card-header">
      <h3>Create a new User</h3>
    </div>
    <div class="card-body">
      {!! Form::open(['route' => 'user.store', 'method' => 'post']) !!}
        <input type="hidden">
        <div class="form-group">
          {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
          {!! Form::text('name', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('lastnames', 'Surnames:', ['class' => 'control-label']) !!}
          {!! Form::text('lastnames', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('username', 'Username:', ['class' => 'control-label']) !!}
          {!! Form::text('username', '', ['class' => 'form-control', 'required' => 'true']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('password', 'Password:', ['class' => 'control-label']) !!}
          {!! Form::password('password', ['class' => 'form-control', 'required' => 'true', 'minlength' => '8']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('admin', 'Admin Role:', ['class' => 'control-label']) !!}
          <select class="form-control" name="admin">
            <option value='0'>Not</option>
            <option value='1'>Yes</option>
          </select>
        </div>
    </div>
    <div class="card-footer">
      <div class="btn-group">
        {!! Form::submit('Save', ['class' => 'btn btn-success']) !!}
        <a href="{{ route('user.index') }}" class="btn btn-secondary">Return</a>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
    
@endsection