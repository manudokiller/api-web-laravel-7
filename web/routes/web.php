<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$user = Http::withToken(session('jwt'))->get(env('API_ME'))->throw()->json();
	if(!array_key_exists('user', $user)){
    return view('welcome')->with(['user' => false]);
  }
  return view('welcome')->with(['user' => true]);
})->name('welcome');


Route::get('auth', 'AuthController@index')->name('auth');
Route::post('login', 'AuthController@login')->name('login');

Route::resource('user', 'UserController')->middleware(['auth','admin']);
Route::resource('client', 'ClientController')->middleware('auth');
Route::resource('contact', 'ContactController')->middleware('auth');
Route::resource('meeting', 'MeetingController')->middleware('auth');
Route::resource('support', 'SupportController')->middleware('auth');
Route::get('logout', 'AuthController@logout')->middleware('auth')->name('logout');
Route::get('home', 'HomeController@index')->middleware('auth')->name('home');
Route::post('report', 'ReportController@report')->middleware('auth')->name('report');